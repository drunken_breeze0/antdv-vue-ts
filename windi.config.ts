import { defineConfig } from "vite-plugin-windicss";
import typography from "windicss/plugin/typography";

export default defineConfig({
  darkMode: "class",
  plugins: [typography()],
  theme: {
    colors: {
      primary: "var(--color-primary)",
      secondary: "var(--color-secondary)",
    },
    fontFamily: {
      sans: ["Open Sans", "ui-sans-serif", "system-ui"],
      serif: ["Montserrat", "ui-serif", "Georgia"],
      mono: ["Fira Sans", "ui-monospace", "SFMono-Regular"],
    },
  },
});
