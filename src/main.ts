import "default-passive-events";
import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./styles/base.less";

// WindiCSS
import "virtual:windi.css";
import "virtual:windi-devtools";

const app = createApp(App);
app.use(router).use(store).mount("#app");
