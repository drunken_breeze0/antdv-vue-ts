import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";
import defineTsx from "@/views/antd/define-tsx.vue";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    redirect: "/tsx",
  },
  {
    path: "/tsx",
    component: defineTsx,
  },
  {
    path: "/tmpl",
    component: () => import("@/views/antd/script-tmpl.vue"),
  },
];

export default createRouter({
  history: createWebHashHistory(),
  routes,
});
